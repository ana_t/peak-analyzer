#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);




}

MainWindow::~MainWindow()
{
    delete ui;
}

QString file1Path, file2Path;

void MainWindow::on_selectFile1Button_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    QStringList fileNames;
    dialog.setNameFilter(tr("Peak text Files (*.dat)"));


    if (dialog.exec())
        fileNames = dialog.selectedFiles();

    file1Path = fileNames.first();
    ui->pathFile1_label->setText(file1Path);
}

void MainWindow::on_selectFile2Button_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    QStringList fileNames;
    dialog.setNameFilter(tr("Peak text Files (*.dat)"));

    if (dialog.exec())
        fileNames = dialog.selectedFiles();
    file2Path = fileNames.first();
    ui->pathFile2_label->setText(file2Path);
}

void MainWindow::on_analyze_Button_clicked()
{
    if(file2Path.isNull() || file1Path.isNull()){
        QMessageBox messageBox;
        messageBox.warning(0,"Error","Please choose both files to compare");
        messageBox.setFixedSize(500,200);
    }
}
